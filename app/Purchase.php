<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    //
    public $timestamps = false;

    public function suppliers()
    {
        return $this->belongsTo('App\Supplier','id_supplier','id');
    }
    public function order_details()
    {
        return $this->belongsToMany('App\Product','purchase_products','id_purchase','id_product')
            ->withPivot('id','qty', 'price','subtotal','received_qty');
    }
}
