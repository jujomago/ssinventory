<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Role;

class UsersController extends Controller
{
    //
    public function index(){
        $users=User::all();
        return view('users.index')->with('users', $users);
    }
    public function create(){
        $roles=Role::all();
        return view('users.form')->with('roles', $roles);
    }
    public function show($id){
        $roles=Role::all();
        $user=User::find($id);
        $rolselected=$user->roles->first();

       return view('users.form')
            ->with('user', $user)
            ->with('roles', $roles)
            ->with('idrolselected',$rolselected);

    }
    public function destroy($id){
        User::destroy($id);
        return redirect('users')->with('status', 'Deleted Succesfully');
    }

    public function store(Request $request){

        $this->validate($request,[
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=>'required|email|unique:users,email,'.$request->id,
            'username'=>'required|unique:users,username,'.$request->id,
            'password'=>'required'
        ]);


        if(isset($request->id)){
            $user=User::find($request->id);
            $user->firstname=$request->firstname;
            $user->lastname=$request->lastname;
            $user->phone=$request->phone;
            $user->address=$request->address;
            $user->email=$request->email;
            $user->username=$request->username;
            $user->password=Hash::make($request->password);

            $user->save();
            $user->roles()->detach();
            $user->roles()->attach($request->rol);

            $message='Updated Successfully';
        }else{
            $user=new User();
            $user->firstname=$request->firstname;
            $user->lastname=$request->lastname;
            $user->phone=$request->phone;
            $user->address=$request->address;
            $user->email=$request->email;
            $user->username=$request->username;
            $user->password=Hash::make($request->password);

            $user->save();
            $user->roles()->attach($request->rol);

            $message='Created Successfully';
        }

        return redirect('users')->with('status', $message);

    }


}
