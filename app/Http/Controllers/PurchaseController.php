<?php

namespace App\Http\Controllers;

use App\Product;
use App\Purchase;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders= Purchase::with('suppliers')->get();
        return view('purchases.index')->with('orders',$orders);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $nextId=DB::table('purchases')->max('id') + 1;
        $suppliers=Supplier::pluck('name','id');
        $products=Product::where('qty_in_stock','>',0)->get();
        return view('purchases.create')
            ->with('suppliers',$suppliers)
            ->with('products',$products)
            ->with('order_number',$nextId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // return $request->all();
        $id_purchase=DB::table('purchases')->insertGetId([
            'order_code'=>$request->po_number,
            'id_supplier'=>$request->id_supplier,
            'id_user'=>Auth::id(),
            'order_date'=> date_format(date_create($request->po_date),'Y-m-d'),
            'notes'=>$request->notes,
            'total'=>$request->total
        ]);


        for ($i = 0; $i < count($request->product_ids); $i++) {
            $product_id=$request->product_ids[$i];
            $unitprice=$request->unitprices[$i];
            $qty=$request->qtys[$i];
            $subtotal=$request->subtotals[$i];
            $received_qty=$request->receiveds[$i];
            DB::table('purchase_products')->insert([
                'id_purchase'=>$id_purchase,
                'id_product'=>$product_id,
                'received_qty'=>$received_qty,
                'qty'=>$qty,
                'price'=> $unitprice,
                'subtotal'=>$subtotal
            ]);
        }

        $message = 'Created Successfully';

        return redirect('purchases')->with('status', $message);
   }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {



        $suppliers=Supplier::pluck('name','id');
        $products=Product::where('qty_in_stock','>',0)->get();
        return view('purchases.edit')
            ->with('suppliers',$suppliers)
            ->with('products',$products)
            ->with('purchase', $purchase);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {

    //    return $request->all();
        $product_ids=array_values(array_filter($request->product_ids));
        $product_ids=$request->product_ids;
     //   return $product_ids;

        $id_purchase=DB::table('purchases')
            ->where('id',$purchase->id)
            ->update([
            'id_supplier'=>$request->id_supplier,
            'id_user'=>Auth::id(),
            'order_date'=> date_format(date_create($request->po_date),'Y-m-d'),
            'notes'=>$request->notes,
            'total'=>$request->total
        ]);

        $productsFound=DB::table('purchase_products')
            ->where('id_purchase',$purchase->id)
            ->pluck('id_product')->toArray();

        for ($i = 0; $i < count($product_ids); $i++) {
            $product_id=$product_ids[$i];
            $unitprice=$request->unitprices[$i];
            $qty=$request->qtys[$i];
            $subtotal=$request->subtotals[$i];

            if(in_array($product_id,$productsFound)){
                $id_order_detail = DB::table('purchase_products')->where([
                    'id_purchase'=> $purchase->id,
                    'id_product'=>$product_id
                ])->value('id');

                DB::table('purchase_products')
                    ->where('id',$id_order_detail)
                    ->update([
                        'qty'=>$qty,
                        'price'=> $unitprice,
                        'subtotal'=>$subtotal
                    ]);
            }else{
                DB::table('purchase_products')->insert([
                   'id_purchase'=>$purchase->id,
                   'id_product'=>$product_id,
                   'qty'=>$qty,
                   'price'=> $unitprice,
                   'subtotal'=>$subtotal
                ]);
            }

        }

        $message = 'Updated Successfully';
        return redirect('purchases')->with('status', $message);

    }

/**
* Remove the specified resource from storage.
*
* @param  \App\Purchase  $purchase
* @return \Illuminate\Http\Response
*/
    public function destroy(Purchase $purchase)
    {
        //
    }

    public function deletOrderDetail($idOrderDetail){
      $del=DB::table('purchase_products')->where('id', $idOrderDetail)->delete();
      return 'success';
    }
    public function receiveDetail(Request $request,$idOrderDetail){

        $upd=DB::table('purchase_products')->where('id', $idOrderDetail)->update([
            'received_qty'=>$request->qty
        ]);



        $idBoxMaster=DB::table('boxes')->insertGetId([
            'id_purchase_product' => $request->id_order_detail,
            'id_purchase' => $request->id_order,
            'capacity' => $request->qty,
            'id_producto' => $request->id_product,
            'sku' => $request->sku,
            'numboxes'=>$request->numboxes
        ]);

        for ($i = 0; $i <$request->numboxes; $i++) {
            $cantidadporcaja=$request->qty/$request->numboxes;
            DB::table('boxes_detail')->insert([
                'id_box' => $idBoxMaster,
                'sku' => $request->sku,
                'qty' => $cantidadporcaja
            ]);
        }
        return $upd;
    }


}
