<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    //
    public function index()
    {
        $permissions = DB::table('permissions')->get();
        return view('permissions.index')->with('permissions', $permissions);
    }

    public function create()
    {
        $menusections = DB::table('categories')
            ->where('id_parent', '<>', 0)
            ->get();
        return view('permissions.form')->with('menusections', $menusections);
    }

    public function show($id)
    {

        $permission = DB::table('permissions')->where('id', $id)->first();
        $menusections = DB::table('categories')
            ->where('id_parent', '<>', 0)
            ->get();

        return view('permissions.form')
            ->with('menusections', $menusections)
            ->with('permission', $permission);

    }

    public function destroy($id)
    {
        DB::table('permissions')->where('id', $id)->delete();
        return redirect('permissions')->with('status', 'Deleted Succesfully');
    }

    public function store(Request $request)
    {

        //   print_r($request->all());

        $this->validate($request, [
            'name' => 'required',
            'id_category' => 'required'
        ]);

        $dataToInsert = $request->all();
        unset($dataToInsert['_token']);

        if (isset($request->id)) {
            DB::table('permissions')
                ->where('id', $request->id)
                ->update($dataToInsert);
            $message = 'Updated Successfully';
        } else {
            DB::table('permissions')->insert($dataToInsert);
            $message = 'Created Successfully';
        }

        return redirect('permissions')->with('status', $message);

    }

    private function _combine_array(&$row, $key, $header)
    {
        $row = array_combine($header, $row);
    }

    public function bulkfile(Request $request)
    {

        $csv = array_map('str_getcsv', file($request->file('file')));
        //    print_r($csv);

        array_walk($csv, function (&$a) use ($csv) {
            $a = array_combine(array_map('strtolower', $csv[0]), $a);
        });
        array_shift($csv); # remove column header

        DB::table('suppliers')->insert($csv);

        /* for ($i = 0; $i < count($csv); $i++) {
               $dataObject = (object) $csv[$i];
            $dataToInsert=[
                'name'=>$dataObject->name,
                'address'=>$dataObject->address,
                'email'=>$dataObject->email,
                'phone'=>$dataObject->phone,
                'description'=>$dataObject->description
            ];
            DB::table('suppliers')->insert($csv[$i]);
        }*/
        return response('The records of file was imported succesfully', 200);
        //return  $csv;
    }


}
