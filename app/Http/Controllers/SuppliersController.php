<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SuppliersController extends Controller
{
    //
    public function index(){
        $suppliers=DB::table('suppliers')->get();
        return view('suppliers.index')->with('suppliers', $suppliers);
    }
    public function show($id){
       $supplier=DB::table('suppliers')->where('id',$id)->first();
       return view('suppliers.form')->with('supplier', $supplier);
     
    }
    public function destroy($id){
        DB::table('suppliers')->where('id',$id)->delete();
        return redirect('suppliers')->with('status', 'Deleted Succesfully');
    }

    public function store(Request $request){

       $this->validate($request,[
            'fullname'=>'required',
            'address'=>'required',
            'email'=>'required|email|unique:suppliers'
       ]);


       $dataToInsert=[
                'name'=>$request->fullname,
                'address'=>$request->address,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'description'=>$request->description
          ];

       if(isset($request->id)){
          DB::table('suppliers')
                ->where('id',$request->id)
                ->update($dataToInsert);
          $message='Updated Successfully';
       }else{
          DB::table('suppliers')->insert($dataToInsert);
           $message='Created Successfully';
       }

       return redirect('suppliers')->with('status', $message);
  
    }

    private function _combine_array(&$row, $key, $header) {
        $row = array_combine($header, $row);
    }

    public function bulkfile(Request $request){

        $csv = array_map('str_getcsv', file($request->file('file')));
    //    print_r($csv);

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine(array_map('strtolower',$csv[0]), $a);
        });
        array_shift($csv); # remove column header

        DB::table('suppliers')->insert($csv);

        /* for ($i = 0; $i < count($csv); $i++) {
               $dataObject = (object) $csv[$i];
            $dataToInsert=[
                'name'=>$dataObject->name,
                'address'=>$dataObject->address,
                'email'=>$dataObject->email,
                'phone'=>$dataObject->phone,
                'description'=>$dataObject->description
            ];
            DB::table('suppliers')->insert($csv[$i]);
        }*/
        return response('The records of file was imported succesfully', 200);
        //return  $csv;
    }


}
