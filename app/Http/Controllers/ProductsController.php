<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategorie;
use App\Supplier;
use Illuminate\Http\Request;
use Monarobase\CountryList\CountryList;
use Monarobase\CountryList\CountryListFacade;
use Monarobase\CountryList\CountryListServiceProvider;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products=Product::with('categories')->where('status', 1)->get();
        return view('products.index')
            ->withProducts($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $categories=ProductCategorie::pluck('name','id');
        $suppliers=Supplier::pluck('name','id');
        return view('products.create')
            ->withCategories($categories)
            ->withSuppliers($suppliers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'userproductname'=>'required',
            //'warehouselocation'=>'required',
            'unitprice'=>'nullable|numeric|min:0',
            'qty_in_stock'=>'nullable|numeric|min:0',
            'qty_p_box'=>'nullable|numeric|min:0',
            'weight'=>'nullable|numeric|min:0',
            'weightoz'=>'nullable|numeric|min:0',
            'length'=>'nullable|nullable|numeric|min:0',
            'width'=>'nullable|numeric|min:0',
            'height'=>'nullable|numeric|min:0',
            'sku'=>'required|unique:products',
           //'fillsku'=>'required',
            //'skualias'=>'required',
            'id_category'=>'required',
            //'upc'=>'required|digits:11',
            //'customdescription'=>'required',
            //'customvalue'=>'required|numeric|min:0',
            //'customtariffno'=>'required|numeric|min:0',
            //'customcountry'=>'required'
        ]);
        $inputs=$request->all();

        $categoria=ProductCategorie::find($request->id_category);
        $product=$categoria->products()->create($inputs);

        return redirect('products')->with('status', 'Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {

        $categories=ProductCategorie::pluck('name','id');
        $suppliers=Supplier::pluck('name','id');
        return view('products.edit')
            ->withProduct($product)
            ->withCategories($categories)
            ->withSuppliers($suppliers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

        $this->validate($request,[
            'name'=>'required',
            'userproductname'=>'required',
            //'warehouselocation'=>'required',
            'unitprice'=>'nullable|numeric|min:0',
            'qty_in_stock'=>'nullable|numeric|min:0',
            'qty_p_box'=>'nullable|numeric|min:0',
            'weight'=>'nullable|numeric|min:0',
            'weightoz'=>'nullable|numeric|min:0',
            'length'=>'nullable|nullable|numeric|min:0',
            'width'=>'nullable|numeric|min:0',
            'height'=>'nullable|numeric|min:0',
            'sku'=>'required|unique:products,sku,'.$product->id,
            //'fillsku'=>'required',
            //'skualias'=>'required',
            'id_category'=>'required',
            /*'upc'=>'required|digits:11',
            'customdescription'=>'required',
            'customvalue'=>'required|numeric|min:0',
            'customtariffno'=>'required|numeric|min:0',
            'customcountry'=>'required'*/
        ]);
        $inputs=$request->all();

        $product->update($inputs);
        return redirect('products')->with('status', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $product->update(['status' => 0]);
         return redirect('products')->with('status', 'Deleted Successfully');
    }

    public function bulkfile(Request $request){

        $csv = array_map('str_getcsv', file($request->file('file')));

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine(array_map('strtolower',$csv[0]), $a);
        });
        array_shift($csv); # remove column header

        $inserted=0;
         for ($i = 0; $i < count($csv); $i++) {
               $dataObject = (object) $csv[$i];
              if(!empty($dataObject->category)){
                    $dataToInsert=[
                      'name'=>$dataObject->name,
                      'status'=>($dataObject->active==='TRUE')?1:0,
                      'sku'=>$dataObject->sku,
                      'warehouselocation'=>$dataObject->warehouselocation,
                      'weightoz'=>$dataObject->weightoz,
                      'weight'=>$dataObject->weight,
                      'customdescription'=>$dataObject->customsdescription,
                      'customvalue'=>empty($dataObject->customsvalue)?0:$dataObject->customsvalue,
                      'customtariffno'=>empty($dataObject->customstariffno)?0:$dataObject->customstariffno,
                      'customcountry'=>$dataObject->customscountry,
                      'upc'=>$dataObject->upc,
                      'fillsku'=>$dataObject->fillsku,
                      'length'=>empty($dataObject->length)?0:$dataObject->length,
                      'width'=>empty($dataObject->width)?0:$dataObject->width,
                      'height'=>empty($dataObject->height)?0:$dataObject->height,
                      'userproductname'=>$dataObject->useproductname,
                      'skualias'=>$dataObject->skualias,
                      'tag1'=>$dataObject->tag1,
                      'tag2'=>$dataObject->tag2,
                      'tag3'=>$dataObject->tag3,
                      'tag4'=>$dataObject->tag4,
                      'tag5'=>$dataObject->tag5
                  ];
                  try
                  {
                      $categoria=ProductCategorie::findOrFail($dataObject->category);
                      $product = $categoria->products()->create($dataToInsert);
                      $inserted++;
                  }
                  catch(ModelNotFoundException $e)
                  {
                      //dd(get_class_methods($e)); // lists all available methods for exception object
                     //dd($e);
                  }
              }
        }
        return response('The records of file was imported succesfully<br/>
          '.$inserted.' records was imported 
', 200);
    }

}
