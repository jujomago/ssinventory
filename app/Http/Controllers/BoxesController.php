<?php

namespace App\Http\Controllers;

use App\Box;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BoxesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boxes=Box::where('status',1)->get();
        //DB::table('boxes_detail')->where('id_box',)
        return view('boxes.index')->with('boxes',$boxes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $products=Product::where('status',1)->pluck('name','id');
        return view('boxes.create')->with('products',$products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'capacity'=>'required',
            'numberofboxes'=>'required'
        ]);
        $inputs=$request->all();

        $product=Product::find($request->id_product);
        $product->boxes()->create($inputs);

        return redirect('boxes')->with('status', 'Created Successfully');

        //Box::create()
    }


    public function generateBoxes(Request $request)
    {

        $orders_details=$request->all();

        for ($i = 0; $i < count($orders_details); $i++) {
            $od=(object)$orders_details[$i];

            $conteo=DB::table('boxes')->where([
                'id_purchase_product'=>$od->id_order_detail,
                'id_purchase'=>$od->id_order,
                'id_producto'=>$od->id_product
            ])->count();
            if($conteo==0) {
                DB::table('boxes')->insert([
                    'id_purchase_product' => $od->id_order_detail,
                    'id_purchase' => $od->id_order,
                    'capacity' => $od->qty,
                    'id_producto' => $od->id_product,
                    'sku' => $od->sku
                ]);
            }
        }
        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function show(Box $box)
    {

        $individualBoxes=DB::table('boxes_detail')->where('id_box',$box->id)->get();

        foreach ($individualBoxes as $indbox) {
            $nameproduct=DB::table('products')->where('sku',$indbox->sku)->value('name');
            $indbox->nameproduct=$nameproduct;
        }
        return view('boxes.details')->withBoxes($individualBoxes);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function edit(Box $box)
    {
        //
        $products=Product::where(['status'=>1])->pluck('name','id');
        return view('boxes.edit')->withBox($box)->withProducts($products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Box $box)
    {
     //   return $box;
        $this->validate($request,[
            'capacity'=>'required',
            'numboxes'=>'required'
        ]);

        DB::table('boxes')
            ->where('id',$box->id)
            ->update([
            'capacity' => $request->capacity,
            'numboxes'=>$request->numboxes
            ]);

        DB::table('boxes_detail')->where('id_box',$box->id)->delete();
        for ($i = 0; $i <$request->numboxes; $i++) {
            $cantidadporcaja=$request->capacity/$request->numboxes;
            DB::table('boxes_detail')->insert([
                'id_box' => $box->id,
                'sku' => $request->id_product,
                'qty' => $cantidadporcaja
            ]);
        }

        return redirect('boxes')->with('status', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function destroy(Box $box)
    {
        //
        $box->update(['status' => 0]);
        return redirect('boxes')->with('status', 'Deleted Successfully');
    }
}
