<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    //
    public function index()
    {
        $roles = DB::table('roles')->get();

        foreach ($roles as $rol) {
            $roles_permission=DB::table('roles_permissions')
                                 ->select('id_permission')
                                 ->where('id_rol',$rol->id)
                                ->get();
            foreach ($roles_permission as $rp){
                $rp->name=DB::table('permissions')->select('name')->where('id',$rp->id_permission)->first()->name;
            }
            $rol->permissions=$roles_permission;
        }

       return view('roles.index')->with('roles', $roles);
    }
    public function show($id){

        $rol=DB::table('roles')->where('id',$id)->first();
        $permissions = DB::table('permissions')
            ->get();
        $permissions_selected_ids=DB::table('roles_permissions')
                                    ->where('id_rol',$id)
                                    ->pluck('id_permission')->all();
        return view('roles.form')
                ->with('rol',  $rol)
                ->with('permissions',$permissions)
                ->with('permissions_selected_ids',$permissions_selected_ids);
    }
    public function create()
    {
        $permissions = DB::table('permissions')
                       ->get();
        return view('roles.form')->with('permissions',$permissions);
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required'
        ]);

        if (isset($request->id)) {
            DB::table('roles')
                ->where('id', $request->id)
                ->update(['name'=>$request->name]);
            $message = 'Updated Successfully';

            DB::table('roles_permissions')
                ->where('id_rol',$request->id)
                ->delete();

            foreach($request->permissions as $idpermission){
                DB::table('roles_permissions')->insert([
                    'id_rol'=>$request->id,
                    'id_permission'=>$idpermission
                ]);
            }

        } else {
            $rolId=DB::table('roles')->insertGetId(['name'=>$request->name]);
            foreach($request->permissions as $idpermission){
                DB::table('roles_permissions')->insert([
                    'id_rol'=>$rolId,
                    'id_permission'=>$idpermission
                ]);
            }
            $message = 'Created Successfully';
        }

       return redirect('roles')->with('status', $message);
    }
}
