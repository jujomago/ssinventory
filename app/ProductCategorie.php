<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategorie extends Model
{
    //
    protected $table = 'categories_products';
    public $timestamps = false;

    public function products()
    {
        return $this->hasMany('App\Product','id_category','id');
    }

}
