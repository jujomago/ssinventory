<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public $timestamps = false;
    protected $guarded = ['_token','category'];

    /*protected $fillable = [
        'name',
        'userproductname',
        'warehouselocation',
        'sku',
        'fillsku',
        'skualias',
        'upc',
        'customdescription',
        'customvalue',
        'customtariffno',
        'customcountry'
    ];*/

    public function categories()
    {
        return $this->belongsTo('App\ProductCategorie','id_category','id');
    }
    public function boxes()
    {
        return $this->hasMany('App\Box','id_product','id');
    }
    public function pos()
    {
        return $this->belongsToMany('App\Purchase','purchase_products','id_product','id_purchase');
    }
}
