<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    //
    protected $table = 'boxes';
    public $timestamps = false;

    protected $guarded = ['_token'];

    public function products()
    {
        return $this->belongsTo('App\Product','id_product','id');
    }
}
