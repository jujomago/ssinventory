<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
Route::get('admin',function(){
    return view('dashboard');
});
*/
/*
Route::get('admin','DashboradController@admin');

Route::get('roles', 'RolesController@index');
Route::post('roles', 'RolesController@update');
Route::put('roles/1/update', 'RolesController@update');
Route::delete('roles/1/delete', 'RolesController@deactivate');

*/
// implicit controller

Route::group(['prefix'=>'/suppliers','middleware'=>'auth'],function(){
    Route::get('/', 'SuppliersController@index');
    Route::get('/create', function(){
        return view('suppliers.form');
    });
    Route::get('/csvupload', function(){
        return view('_elements.formfile')
            ->with('formaction','/suppliers/bulkfile')
            ->with('backurl','/suppliers');
    });
    Route::get('/{id}', 'SuppliersController@show');
    Route::post('/store', 'SuppliersController@store');
    Route::post('/bulkfile','SuppliersController@bulkfile');
    Route::delete('/delete/{id}', 'SuppliersController@destroy');
});


Route::group(['prefix'=>'/permissions','middleware'=>'auth'],function(){
    Route::get('/','PermissionsController@index');
    Route::get('/create','PermissionsController@create');
    Route::get('/{id}', 'PermissionsController@show');
    Route::post('/store', 'PermissionsController@store');
    Route::delete('/delete/{id}', 'PermissionsController@destroy');
});

Route::group(['prefix'=>'/roles','middleware'=>'auth'],function(){
    Route::get('/','RolesController@index');
    Route::get('/create','RolesController@create');
    Route::get('/{id}', 'RolesController@show');
    Route::post('/store', 'RolesController@store');
    Route::delete('/delete/{id}', 'RolesController@destroy');
});

Route::group(['prefix'=>'/users','middleware'=>'auth'],function(){
    Route::get('/','UsersController@index');
    Route::get('/create','UsersController@create');
    Route::get('/{id}', 'UsersController@show');
    Route::post('/store', 'UsersController@store');
    Route::delete('/delete/{id}', 'UsersController@destroy');
});





Route::group(['middleware' => 'auth'], function() {
    Route::resource('boxes','BoxesController');
    Route::post('/boxes/generate','BoxesController@generateBoxes');


    Route::get('/products/csvupload', function(){
        return view('_elements.formfile')
            ->with('formaction','/products/bulkfile')
            ->with('backurl','/products');
    });
    Route::post('/products/bulkfile','ProductsController@bulkfile');
    Route::resource('products','ProductsController');
    Route::resource('purchases','PurchaseController');
    //Route::get('/purchases/ajax/fetchProduct/{id}','PurchaseController@fetchProduct');
    Route::delete('/purchases/orderdetail/{idOrderDetail}','PurchaseController@deletOrderDetail');
    Route::post('/purchases/orderdetail/{idOrderDetail}','PurchaseController@receiveDetail');
});



Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/home', 'HomeController@index');
