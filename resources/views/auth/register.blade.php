@extends('_layouts.outside')

@section('content')
    <div id="register" class="animate form">
        <section class="login_content">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <h1>Create Account</h1>
                <div>
                    <input type="text" class="form-control" placeholder="First Name"  name="firstname" value="{{ old('firstname') }}" required autofocus/>
                    @if ($errors->has('firstname'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                    @endif
                </div>
                <div>
                    <input id="email" type="email"   placeholder="Email"  class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div>
                    <input type="text" class="form-control" placeholder="Username"  name="username" value="{{ old('username') }}" required autofocus/>
                    @if ($errors->has('username'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                    @endif
                </div>
                <div>
                    <input id="password" type="password"   placeholder="Password"   class="form-control" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div>
                    <input id="Confirm Password" type="password"  placeholder="Confirm Password" class="form-control" name="password_confirmation" required>
                </div>
                <div>
                    <button class="btn btn-default submit" href="index.html">Submit</button>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <p class="change_link">Already a member ?
                        <a href="/login" class="to_register"> Log in </a>
                    </p>

                    <div class="clearfix"></div>
                    <br />
                    <div>
                        <p>©2017 All Rights Reserved. Privacy and Terms</p>
                    </div>
                </div>
            </form>
        </section>
    </div>
@endsection
