@extends('_layouts.outside')

@section('content')
    <div class="animate form login_form">
        <section class="login_content">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <h1>Login Form</h1>

                <div>
                    <input id="username" type="text" placeholder="Username" class="form-control" name="username" value="{{ old('username') }}" autofocus>
                    @if ($errors->has('username'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                    @endif
                </div>
                <div>
                    <input id="password" type="password"  placeholder="Password" class="form-control" name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div>
                    <button type="submit" class="btn btn-default submit">Log in</button>
                    {{--<a class="reset_pass" href="#">Lost your password?</a>--}}
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <p class="change_link">New to site?
                        <a href="/register" class="to_register"> Create Account </a>
                    </p>

                    <div class="clearfix"></div>
                    <br />
                    <div>
                        <p>©2017 All Rights Reserved. Privacy and Terms</p>
                    </div>
                </div>
            </form>
        </section>
    </div>
@endsection
