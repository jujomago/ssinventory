@extends('_layouts.blankpage')
@section('page-title','Boxes List')

@section('default-stylesheets')
    @parent
    <!-- Datatables -->
    <link href="/gentella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

@endsection

@section('default-scripts')
    @parent
    <!-- Datatables -->
    <script src="/gentella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/gentella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="x_panel"
            <div class="x_title">
                <h2>Purchase Orders
                    <small>List</small>
                </h2>
                <a href="/purchases/create" class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil"></i> Create Purchase Order</a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable-buttons" class="table table-striped">
                    <thead>
                    <tr>
                        <th>Supplier</th>
                        <th>PO Number</th>
                        <th>Date</th>
                        <th>Total</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $po)
                        <tr>
                            <td>{{$po->suppliers->name}}</td>
                            <td>{{$po->order_code}}</td>
                            <td>{{date_format(date_create($po->order_date),'d/m/Y')}}</td>
                            <td>{{$po->total}}</td>
                            <td>
                                <a href="/purchases/{{$po->id}}/edit" class="btn btn-info btn-xs"><i
                                            class="fa fa-pencil"></i> Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection