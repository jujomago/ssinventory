@extends('_layouts.blankpage')

@section('page-title','Create a Purchase Order')

@section('default-stylesheets')
    @parent
    <!-- bootstrap-daterangepicker -->
    <link href="/gentella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('default-scripts')
    @parent
    <!-- Parsley -->
    <script src="/gentella/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/gentella/vendors/moment/min/moment.min.js"></script>
    <script src="/gentella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection
@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="x_panel">
            <div class="x_title">
                <h2>New Purchase Order</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br/>
                {!! Form::open([
                    'url'=>'/purchases',
                    'class'=>'form-horizontal form-label-left',
                    'id'=>'demo-form2',
                    'data-parsley-validate'
                    ]) !!}

                <div class="form-group">
                    {{ Form::label('po_number','Po Number ',['class'=>'control-label col-sm-2 col-xs-12']) }}
                    <div class="col-sm-3 col-xs-12">
                        {{Form::number('po_number',$order_number,['class'=>'form-control col-xs-12','readonly'])}}
                    </div>
                    {{ Form::label('order_date','Order Date',['class'=>'control-label col-sm-3 col-xs-12']) }}
                    <div class="col-sm-3 col-xs-12 xdisplay_inputx">
                        <input type="text" name="po_date" class="form-control col-md-7 col-xs-12 has-feedback-left" id="single_cal1" placeholder="First Name" aria-describedby="inputSuccess2Status">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        <span id="inputSuccess2Status" class="sr-only">(success)</span>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('id_supplier','Supplier *',['class'=>'control-label col-sm-2 col-xs-12']) }}
                    <div class="col-xs-9">
                        {{ Form::select('id_supplier',
                                       $suppliers,
                                       null,
                                      ['placeholder' => 'Pick a Supplier..','class'=>'form-control', 'required']
                                      )
                                    }}

                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('notes','Notes',['class'=>'control-label col-sm-2 col-xs-12']) }}
                    <div class="col-xs-9">
                        {{Form::textarea('notes','',['class'=>'form-control col-xs-12', 'rows'=>2])}}
                    </div>
                </div>
                <div class="table-responsive" id="rowscontainer">
                    <table class="table table-striped jambo_table">
                        <thead>
                            <tr class="headings">
                                <th class="column-title col-xs-3">SKU</th>
                                <th class="column-title  col-xs-4">Name</th>
                                <th class="column-title  col-xs-2">Unit price</th>
                                <th class="column-title  col-xs-2">Qty</th>
                                <th class="column-title  col-xs-1">SubTotal</th>
                                <th class="">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr is="order-row"
                                :products="{{ $products }}"
                                v-for="(item, index) in counter"></tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4"><strong>Total</strong></td>
                                <td colspan="2">@{{sumTotal}}</td>
                            </tr>
                        </tfoot>
                    </table>
                    <input type="hidden" name="total" v-model="sumTotal">
                    <button type="button" class="btn btn-primary btn-small" @click="addRow">Add Row</button>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-10">
                        <button class="btn btn-primary" type="button">Cancel</button>
                        {!! Form::submit('Submit!',["class"=>"btn btn-success"]) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection