@extends('_layouts.blankpage')

@section('page-title','Create a Rol')

@section('default-stylesheets')
    @parent
    <!-- iCheck -->
    <link href="/gentella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="/gentella/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="/gentella/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="/gentella/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="/gentella/vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="/gentella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('default-scripts')
    @parent
    <script src="/gentella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/gentella/vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/gentella/vendors/moment/min/moment.min.js"></script>
    <script src="/gentella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="/gentella/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="/gentella/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="/gentella/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="/gentella/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="/gentella/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="/gentella/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="/gentella/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="/gentella/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="/gentella/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="/gentella/vendors/starrr/dist/starrr.js"></script>

@endsection
@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="x_panel">
            <div class="x_title">

                @if(isset($rol))
                    <h2>Edit Rol :
                        <small>{{$rol->name}}</small>
                    </h2>
                @else
                    <h2>New Rol</h2>
                @endif

                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br/>
                <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post"
                      action="/roles/store">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-xs-12" for="fullname">Name<span
                                    class="required">*</span>
                        </label>
                        <div class="col-sm-6 col-xs-12">
                            <input type="text" id="fullname" name="name" required="required"
                                   class="form-control col-md-7 col-xs-12"
                                   value="{{isset($rol)?$rol->name:''}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-sm-3 col-xs-12">Permissions Assigned</label>
                        <div class="col-xs-9 permissionbox">
                            @foreach($permissions as $permission)
                                <div class="checkbox">
                                    <label data-toggle="tooltip" data-placement="top" title="{{$permission->description}}">
                                        <input type="checkbox" class="flat"  name="permissions[]" value="{{$permission->id}}"
                                                @isset($rol)
                                                    @if(in_array($permission->id,$permissions_selected_ids))
                                                        checked
                                                    @endif
                                                @endisset
                                        > {{$permission->name}}
                                    </label>
                                </div>
                            @endforeach
                        </div>

                    </div>

                    @isset($rol)
                        <input type="hidden" name="id" value="{{$rol->id}}">
                    @endisset

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button class="btn btn-primary" type="button">Cancel</button>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection