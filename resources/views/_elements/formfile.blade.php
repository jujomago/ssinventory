@extends('_layouts.blankpage')

@section('page-title','CSV file Upload')

@section('default-stylesheets')
    @parent
    <!-- Dropzone.js -->
    <link href="/gentella/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
@endsection
@section('default-scripts')
    @parent
    <!-- Dropzone.js -->
    <script src="/gentella/vendors/dropzone/dist/min/dropzone.min.js"></script>
@endsection
@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form CSV Upload</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p>Drag CSV file to the box below  or click to select the file..</p>
                <form action="{{$formaction}}" class="dropzone" id="form-csv-file">
                    {{ csrf_field() }}
                </form>
                <div id="msgformupload" class="hidden">
                    <div class="alert alert-success ">
                        The records of the file were imported Successfully!
                    </div>
                    <a href="{{$backurl}}" class="btn btn-warning btn-large">Go to List</a>
                </div>
            </div>
        </div>
    </div>
@endsection