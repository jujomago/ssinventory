        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>SS Inventory!</span></a>
            </div>

            <div class="clearfix"></div>
          @php
            if(Auth::check()){
            $rolUser= \App\User::find(Auth::id())->roles->first();
            $ids_permissions=DB::table('roles_permissions')->where('id_rol',$rolUser->id)->pluck('id_permission')->all();
            $permissions=DB::table('permissions')->get();
            }
          @endphp
            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="/gentella/images/unnamed.png" alt="..." class="img-circle profile_img">
              </div>

              @if(Auth::check())
              <div class="profile_info">
                <span>Rol: {{ $rolUser->name }}</span>
                <h2>{{Auth::user()->firstname}} {{Auth::user()->lastname}}</h2>
              </div>
              @endif
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

              <div class="menu_section">

                <ul class="nav side-menu">
                  @if(Auth::check())
                  @foreach($permissions as $pm)
                    @if(in_array($pm->id,$ids_permissions))
                      <li><a href="{{$pm->url}}"><i class="fa {{$pm->icon}}"></i> {{$pm->name}}</a></li>
                    @endif
                  @endforeach
                    @endif
                </ul>

                {{--
                                <h3>Inventory</h3>


                                <ul class="nav side-menu">
                                  <li><a><i class="fa fa-edit"></i> Suppliers <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                      <li><a href="/suppliers">Listing</a></li>
                                      <li><a href="/suppliers/create">Add Supplier</a></li>
                                      <li><a href="form_validation.html">CSV Upload</a></li>
                                    </ul>
                                  </li>
                                  <li><a><i class="fa fa-edit"></i> Products <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                      <li><a href="/suppliers">Listing</a></li>
                                      <li><a href="/suppliers/create">Add Supplier</a></li>
                                      <li><a href="form_validation.html">CSV Upload</a></li>
                                    </ul>
                                  </li>
                                </ul>--}}

              </div>
              {{--
                <div class="menu_section">
                <h3>System</h3>
                <ul class="nav side-menu">
                   <li><a><i class="fa fa-table"></i> Users <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="tables.html">Tables</a></li>
                      <li><a href="tables_dynamic.html">Table Dynamic</a></li>
                    </ul>
                  </li>  
                  <li><a><i class="fa fa-desktop"></i> Roles <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="general_elements.html">General Elements</a></li>
                      <li><a href="media_gallery.html">Media Gallery</a></li>
                      <li><a href="typography.html">Typography</a></li>
                      <li><a href="icons.html">Icons</a></li>
                      <li><a href="glyphicons.html">Glyphicons</a></li>
                      <li><a href="widgets.html">Widgets</a></li>
                      <li><a href="invoice.html">Invoice</a></li>
                      <li><a href="inbox.html">Inbox</a></li>
                      <li><a href="calendar.html">Calendar</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Permissions <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/catetories">Categories Grouping</a></li>
                      <li><a href="/permissions">Listing</a></li>
                      <li><a href="/permissions/create">Create</a></li>
                    </ul>
                  </li>  
               
                </ul>
              </div>
              <div class="menu_section">
                <h3>Reporting</h3>
                <ul class="nav side-menu">
                  <li><a href="#"><i class="fa fa-bug"></i> Detailed Transaction Report</a></li>
                  <li><a href="#"><i class="fa fa-bug"></i> Inventory Summary Report </a></li>
                </ul>
              </div>
--}}
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>