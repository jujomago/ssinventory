<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('page-title')</title>
    
    @section('default-stylesheets')
          <link href="/gentella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> 
          <!-- Font Awesome -->
          <link href="/gentella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
          <!-- NProgress -->
          <link href="/gentella/vendors/nprogress/nprogress.css" rel="stylesheet">

       
    @show
  
    
    <!-- Custom Theme Style -->
    <link href="/gentella/build/css/custom.min.css" rel="stylesheet"> 
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <!-- Scripts -->
      <script>
          window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
      </script>
  </head>

  <body class="nav-md">
  
    <div class="container body">
      <div class="main_container">
       @include('_elements.sidemenu')	
        <!-- top navigation -->
       @include('_elements.topmenu')	
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          @yield('content')         
        </div>
        <!-- /page content -->

        <!-- footer content -->
          @include('_elements.footer')	
        <!-- /footer content -->
      </div>
    </div>

    @section('default-scripts')
      <!-- jQuery -->
      <script src="/gentella/vendors/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap -->
      <script src="/gentella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- FastClick -->
      <script src="/gentella/vendors/fastclick/lib/fastclick.js"></script>
      <!-- NProgress -->
      <script src="/gentella/vendors/nprogress/nprogress.js"></script>    
      <!-- Custom Theme Scripts -->
     
    @show
  


   
     <script src="/gentella/build/js/custom.js"></script>
     <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>
