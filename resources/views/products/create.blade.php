@extends('_layouts.blankpage')

@section('page-title','Create a Product')


@section('default-scripts')
    @parent
    <!-- Parsley -->
    <script src="/gentella/vendors/parsleyjs/dist/parsley.min.js"></script>

@endsection
@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="x_panel">
            <div class="x_title">
                <h2>New Product</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br/>
                {!! Form::open([
                    'url'=>'/products',
                    'class'=>'form-horizontal form-label-left',
                    'id'=>'demo-form2',
                    'data-parsley-validate'
                    ]) !!}
                <fieldset>
                    <legend>Item</legend>
                    <div class="form-group">
                        {{ Form::label('name','Name *',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('name','',['class'=>'form-control col-md-7 col-xs-12','required'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('userproductname','Use Product Name *',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('userproductname','',['class'=>'form-control col-md-7 col-xs-12','required'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('warehouselocation','Warehouse Location',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('warehouselocation','',['class'=>'form-control col-md-7 col-xs-12',])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('unit_price','Unit Price',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-2 col-xs-12">
                            {{Form::number('unit_price','',['min'=>0,'step'=>'any','class'=>'form-control col-md-7 col-xs-12',])}}
                        </div>
                        {{ Form::label('qty_in_stock','Stock',['class'=>'control-label col-sm-2 col-xs-12']) }}
                        <div class="col-sm-2 col-xs-12">
                            {{Form::number('qty_in_stock','',['min'=>0,'class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('qty_p_box','Qty per Box',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-2 col-xs-12">
                            {{Form::number('qty_p_box','',['min'=>0,'class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                        {{ Form::label('weight','Weight',['class'=>'control-label col-sm-2 col-xs-12']) }}
                        <div class="col-sm-2 col-xs-12">
                            {{Form::number('weight','',['min'=>0,'step'=>'any','class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('weightoz','Weight(Oz)',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-2 col-xs-12">
                            {{Form::number('weightoz','',['min'=>0,'step'=>'any','class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                        {{ Form::label('length','Length(cm)',['class'=>'control-label col-sm-2 col-xs-12']) }}
                        <div class="col-sm-2 col-xs-12">
                            {{Form::number('length','',['min'=>0,'step'=>'any','class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('width','Width(cm)',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-2 col-xs-12">
                            {{Form::number('width','',['min'=>0,'step'=>'any','class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                        {{ Form::label('height','Height(cm)',['class'=>'control-label col-sm-2 col-xs-12']) }}
                        <div class="col-sm-2 col-xs-12">
                            {{Form::text('height','',['min'=>0,'step'=>'any','class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('sku','SKU *',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('sku','',['class'=>'form-control col-md-7 col-xs-12','required'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('fillsku','FillSKU',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('fillsku','',['class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('skualias','SKU Alias',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('skualias','',['class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('upc','UPC',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('upc','',[
                                'class'=>'form-control col-md-7 col-xs-12',
                                'maxlength'=>11,
                                'data-parsley-type'=>"digits",
                                ])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('id_category','Category *',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{ Form::select('id_category',
                                 $categories,
                                 null,
                                ['placeholder' => 'Pick a category...','class'=>'form-control col-md-7 col-xs-12','required']
                                )
                              }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('id_supplier','Supplier',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{ Form::select('id_supplier',
                                 $suppliers,
                                 null,
                                ['placeholder' => 'Pick a supplier...','class'=>'form-control col-md-7 col-xs-12']
                                )
                              }}
                        </div>
                    </div>

                </fieldset>

                <fieldset>
                    <legend>Customs</legend>
                    <div class="form-group">
                        {{ Form::label('customdescription','Custom Description',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('customdescription','',['class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('customvalue','Custom Value',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::number('customvalue','',['min'=>0,'step'=>'any','class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('customtariffno','Custom Tariff No',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::number('customtariffno','',['min'=>0,'class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('customcountry','Custom Country',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                           {{ Form::select('customcountry',
                                Countries::getList('en', 'php'),
                                //['L' => 'Large', 'S' => 'Small'],
                                null,
                               ['placeholder' => 'Pick a country...','class'=>'form-control col-md-7 col-xs-12']
                               )
                             }}
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Tags</legend>
                    <div class="form-group">
                        {{ Form::label('tag1','Tag 1',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('tag1','',['class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('tag2','Tag 2',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('tag2','',['class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('tag3','Tag 3',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('tag3','',['class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('tag4','Tag 4',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('tag4','',['class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('tag5','Tag 5',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('tag5','',['class'=>'form-control col-md-7 col-xs-12'])}}
                        </div>
                    </div>

                </fieldset>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button class="btn btn-primary" type="button">Cancel</button>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            {!! Form::submit('Submit!',["class"=>"btn btn-success"]) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection