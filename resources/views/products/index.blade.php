@extends('_layouts.blankpage')
@section('page-title','Products List')

@section('default-stylesheets')
    @parent
    <!-- Datatables -->
    <link href="/gentella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

@endsection

@section('default-scripts')
    @parent
    <!-- Datatables -->
    <script src="/gentella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/gentella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="x_panel"
            <div class="x_title">
                <h2>Products
                    <small>List</small>
                </h2>
                <a href="/products/csvupload" class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil"></i> CSV File Upload</a>
                <a href="/products/create" class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil"></i> Add
                    Product</a>

                <div class="clearfix"></div>

            </div>
            <div class="x_content">
                <table id="datatable-buttons" class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>SKU</th>
                        <th>UPC</th>
                        <th>Unit Price</th>
                        <th>Category</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->sku}}</td>
                            <td>{{$product->upc}}</td>
                            <td>{{$product->unit_price}}</td>
                            <td>{{$product->categories->name}}</td>
                            <td>
                                <a href="/products/{{$product->id}}/edit" class="btn btn-info btn-xs"><i
                                            class="fa fa-pencil"></i> Edit</a>
                                <a href="javascript:void(0);" onclick="$(this).find('form').submit();"
                                   class="btn btn-danger btn-xs">
                                    <form action="/products/{{$product->id}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                    </form>
                                    <i class="fa fa-trash-o"></i> Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection