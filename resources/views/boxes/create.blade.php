@extends('_layouts.blankpage')

@section('page-title','Create a Box')


@section('default-scripts')
    @parent
    <!-- Parsley -->
    <script src="/gentella/vendors/parsleyjs/dist/parsley.min.js"></script>

@endsection
@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="x_panel">
            <div class="x_title">
                <h2>New Box</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br/>
                {!! Form::open([
                    'url'=>'/boxes',
                    'class'=>'form-horizontal form-label-left',
                    'id'=>'demo-form2',
                    'data-parsley-validate'
                    ]) !!}
                    <div class="form-group">
                        {{ Form::label('id_product','Product *',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{ Form::select('id_product',
                                 $products,
                                 null,
                                ['placeholder' => 'Pick a Product...','class'=>'form-control col-md-7 col-xs-12','required']
                                )
                              }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('capacity','Capacity *',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('capacity','',['class'=>'form-control col-md-7 col-xs-12','required'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('numberofboxes','Number of Boxes *',['class'=>'control-label col-sm-3 col-xs-12']) }}
                        <div class="col-sm-6 col-xs-12">
                            {{Form::text('numberofboxes','',['class'=>'form-control col-md-7 col-xs-12','required'])}}
                        </div>
                    </div>





                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button class="btn btn-primary" type="button">Cancel</button>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            {!! Form::submit('Submit!',["class"=>"btn btn-success"]) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection