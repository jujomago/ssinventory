@extends('_layouts.blankpage')
@section('page-title','Boxes List')

@section('default-stylesheets')
    @parent
    <!-- Datatables -->
    <link href="/gentella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

@endsection

@section('default-scripts')
    @parent
    <!-- Datatables -->
    <script src="/gentella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/gentella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="x_panel"
            <div class="x_title">
                <h2>Boxes
                    <small>List</small>
                </h2>
                <a href="/boxes/create" class="btn btn-primary btn-sm pull-right"><i class="fa fa-pencil"></i> Add
                    Box</a>

                <div class="clearfix"></div>

            </div>
            <div class="x_content">
                <table id="datatable-buttons" class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <td>PO Number</td>
                        <th>Product (SKU)</th>
                        <th>Capacity</th>
                        <th>Number of Boxes</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($boxes as $box)
                            <tr>
                                <td>{{$box->id}}</td>
                                <td>{{$box->id_purchase}}</td>
                                <td>{{$box->sku}}</td>
                                <td>{{$box->capacity}}</td>
                                <td>{{$box->numboxes}}</td>
                                <td>
                                    <a href="/boxes/{{$box->id}}/edit" class="btn btn-info btn-xs"><i
                                                class="fa fa-pencil"></i> Edit</a>
                                    <a href="/boxes/{{$box->id}}" class="btn btn-warning btn-xs">View Box Details</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection