@extends('_layouts.blankpage')
@section('page-title','Boxes List')

@section('default-stylesheets')
    @parent
    <!-- Datatables -->
    <link href="/gentella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

@endsection

@section('default-scripts')
    @parent
    <!-- Datatables -->
    <script src="/gentella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/gentella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel"
            <div class="x_title">
                <h2> Individual Boxes
                    <small>List</small>
                </h2>
            </div>
            <div class="x_content">
                @foreach($boxes as $box)
                    <div class="col-xs-6">
                        <div class="well text-center">
                            <h2>{{$box->sku}}</h2>
                            <h2>{{$box->nameproduct}}</h2>
                            <h2>Cap: {{$box->qty}}</h2>
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG(($box->id.$box->sku.$box->qty), 'C128',2,80) }}" class="img-responsive center-block" alt="barcode" />
                            <br>
                            <strong>{{$box->id}} - {{$box->sku}} - {{$box->qty}}</strong><br>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection