/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
//window._ = require('lodash');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
/*
 Vue.component('example', require('./components/Example.vue'));*/
/*
 const app = new Vue({
 el: '#app'
 });*/


Vue.component('order-row', require('./components/OrderRow.vue'));


window.appOrder  = new Vue({
    el: '#rowscontainer',
    data: {
        counter: 0,
        pchildren: [],
        generated:false
    },
    mounted () {
        console.log('MOUNTED PARENT');
    //    console.log(...this.$children);
       this.pchildren.push(...this.$children);
       this.pchildren = this.$children;
        if(this.pchildren.length==0){
            this.counter=1;
        }
    },
    methods: {
        addRow(){
            this.counter++;
        },
        genBoxes(){
            let _self=this;
            let boxes=[];
            for (var i in this.pchildren) {
                let productRow=this.pchildren[i];
                if(productRow.visible && productRow.activeReceived){
                    boxes.push({
                         'id_order_detail':productRow.selectedProduct.pivot.id,
                         'id_order':productRow.selectedProduct.pivot.id_purchase,
                         'id_product':productRow.selectedProduct.id,
                         'sku':productRow.skutemp,
                         'qty':productRow.qty
                    });
                }
            }
            console.log('boxes:',boxes);

            axios.post(`/boxes/generate`,boxes)
                .then(function (response) {
                    if(response.status==200 && response.data==1){
                        _self.generated=true;
                    }
                })
                .catch(function (error) {
                    console.log('error gen Boxes');
                    console.log(error);
                });
        }
    },
    computed:{
        sumTotal(){
            console.log('entro aqui');
            let sum=0;
            let items = this.pchildren;
        //    console.log(items.length);

            for (var i in items) {
                if(items[i].visible){
                    console.log(items[i].subtotal);
                    sum+= Number(items[i].subtotal);
                }
            }
            return sum;
        },
        exitsReceived(){
            for (var i in this.pchildren) {
                let productRow=this.pchildren[i];
                return (productRow.visible && productRow.activeReceived);
            }
        },
        missingReceiveds(){
            let totalOrders=_.filter(this.pchildren,{'visible':true}).length;
            let c=0;
            for (var i in this.pchildren) {
                let productRow=this.pchildren[i];
                if(productRow.visible && productRow.activeReceived){
                    c++;
                }
            }
            return (c!=totalOrders);
        }
    }
});


if (typeof Dropzone != 'undefined') {
    Dropzone.options.formCsvFile = {
        maxFiles: 1,
        maxFilesize: 2, // MB
        acceptedFiles: '.csv,.txt',
        uploadMultiple: false,
        init: function () {
            this.on("error", function (eMsg) {
                console.log('error');
                console.log(eMsg);
            });
            this.on("success", function (file, data) {
                console.log('sucddcess');
                console.log(data);
                $("#msgformupload").find('.alert').html(data);
                $("#msgformupload").removeClass('hidden').fadeIn();
            });
        }
    };
}
/*
var $currEl, $inputUnitPrice, $inputQty;

$('.productrow').on('change', (e) => {
    $currEl = $(e.currentTarget);
    let curval = $currEl.val();
    $inputUnitPrice = $currEl.parent().next().next().find('input');
    $inputQty = $currEl.parent().next().next().next().find('input');
    $inputUnitPrice.on('keyup', sumSubTotal);
    $inputQty.on('keyup', sumSubTotal);

    $.get('/purchases/ajax/fetchProduct/' + curval, function (data) {
        // console.log(data);
        var unitPrice = data.unit_price;
        var sku = data.sku;
        $currEl.parent().next().html(sku);
        $inputUnitPrice.val(unitPrice);
        sumSubTotal(e);
    });
});

function sumSubTotal(e) {
    console.log(e);
    subTotal = $inputQty.val() * $inputUnitPrice.val();
    $(e.currentTarget).parent().nextAll('td').last().html(subTotal);

}*/